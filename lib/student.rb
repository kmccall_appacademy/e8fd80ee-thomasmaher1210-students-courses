class Student

  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    return if courses.include?(course)
    raise "course conflict" if has_conflict?(course)

    self.courses << course
    course.students << self
  end

  def has_conflict?(new_c)
    self.courses.any? do |enrolled|
      new_c.conflicts_with?(enrolled)
    end

  end

  def course_load
    credit_by_dept = Hash.new(0)
    self.courses.each {|d| credit_by_dept[d.department] += d.credits }
    credit_by_dept
  end

end
